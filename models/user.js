const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const Schema = mongoose.Schema;// We use this to tell mongoose about the very particular fields that our models going to have
//our model
const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true
  },
  password: String
});

userSchema.pre('save', function (next) {
  const user = this;//get access to the user model
  //generate a salt then run callback
  bcrypt.genSalt(10, function(err, salt) {
    if (err) { return next(err); }
    //hash (encrypt) our password using the salt
    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) { return next(err); }
      //overwrite plain text password with encrypted password
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function(candidatePassword, callback) {//compare the password sent by the user, we hash this with the salt
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {// and after that, we compared with our stored (hashed with the salt) password
    if (err) { return callback(err); }

    callback(null, isMatch);
  });
}
//create model class
const ModelClass = mongoose.model('user', userSchema);

module.exports = ModelClass;