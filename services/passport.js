const passport = require('passport');
const User = require('../models/user');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, function(email, password, done) {
// email and password ok? call "done"
  User.findOne({ email: email }, function(err, user) {
    if (err) { return done(err, false); } // err? call "done" with false value

    if (!user) { return done(null, false); } // not? call "done" with false value

    // password is equal to user.password?

    user.comparePassword(password, function(err, isMatch) {
      if (err) { return done(err); }
      if (!isMatch) { return done(null, false); }

      return done(null, user);
    });
  });
});

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.secret
};
//payload is the decoded jwt token
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
  //the user id in the payload exists in out db?
  User.findById(payload.sub, function(err, user) {
    if (err) { return done(err, false); } //otherwise, call done without a user object

    if (user) {//does exists? call "done" with that user
      done(null, user);
    } else {// no error, no user
      done(null, false);
    }
  });
});

passport.use(jwtLogin);
passport.use(localLogin);