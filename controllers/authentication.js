const jwt = require('jwt-simple');
const config = require('../config');
const User = require('../models/user');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signIn = function (req, res, next) {
  //we just need to give a token to the user
  res.send({ token: tokenForUser(req.user) });
}

exports.signUp = function (req, res, next) {
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res.status(422).send({ err: 'You must provide email and password' });
  }
  // user exists?
  User.findOne({ email: email }, function (err, existingUser) {
    if (err) { return next(err); }

    if (existingUser) {//exist? error
      return res.status(422).send({ err: 'this e-mail is already in use' });
    }//422 unprocessable entity
    //no? create it
    const user = new User({ email: email, password: password });
    user.save(function (err) {
      if (err) { return next(err); }
      //respond "user created"
      res.json({ token: tokenForUser(user) });
    });
  });
}